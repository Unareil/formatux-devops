Découvrir la philosophie **devops**.

.Objectifs pédagogiques
****
icon:tags[] **devops**, **automatisation**, **gestion de configuration**,
 **intégration continue**, **déploiement continu**, **DSL**.

Connaissances : icon:cogs[] icon:cogs[] icon:cogs[]

Niveau technique : icon:star[]

Temps de lecture : 5 minutes
****

Le mouvement *indexterm2:[devops]* cherche à optimiser le travail de toutes
les équipes intervenant sur un système d'information.

* Les développeurs (les **__dev__**) cherchent à ce que leurs applications
soient déployées le plus souvent et le plus rapidement possible.

* Les administrateurs systèmes, réseaux ou de bases de données (les **__ops__**)
cherchent à garantir la stabilité, la sécurité de leurs systèmes
et leur disponibilité.

Les objectifs des *dev* et des *ops* sont donc bien souvent opposés,
la communication entre les équipes parfois difficile :
les dev et les ops n'utilisent pas toujours les mêmes éléments de langage.

* Il n'y a rien de plus frustrant pour un développeur que de devoir attendre
la disponibilité d'un administrateur système pour voir la nouvelle
fonctionnalité de son application être mise en ligne ;

* Quoi de plus frustrant pour un administrateur système de devoir déployer
une nouvelle mise à jour applicative manuellement alors qu'il vient de finir
la précédente ?

La philosophie devops regroupe l'ensemble des outils des deux mondes,
offre un langage commun, afin de faciliter le travail des équipes avec
comme objectif la performance économique pour l'entreprise.

Le travail des développeurs et des administrateurs doit être simplifié
afin d'être automatisé avec des outils spécifiques.

== Le vocabulaire DEVOPS

* le *indexterm2:[build]* : concerne la conception de l'application ;
* le *indexterm2:[run]* : concerne la maintenance de l'application ;
* le *indexterm2:[change]* : concerne l'évolution de l'application.
* l'**indexterm2:[intégration continue]** (__indexterm2:[Continuous Integration]
indexterm2:[CI]__) :
chaque modification d'un code source entraîne une vérification de non-régression
de l'application.

.Schéma de fonctionnement de l'intégration continue
image::000-Integration-continue-001.png[scaledwidth=60%]

* *indexterm2:[automation]* (__automatisation__) : fonctionnement d'un système
sans intervention humaine, automatisation d'une suite d'opération.

* **indexterm2:[idempotence]** : une opération est idempotente si elle a le même
effet quelle soit appliquée une ou plusieurs fois.
Les outils de gestion de configuration sont généralement idempotent.

* **indexterm2:[orchestration]** : processus automatique de gestion d'un
système informatique.

* **indexterm2:[provisionning]** : processus d'allocation automatique de
s ressources.

.Pourquoi scripter en Bash n'est pas considéré comme de l'automatisation ?
****
Les langages impératifs contre les languages déclaratifs...

Même si la connaissance du *Bash* est une exigence de base pour un
administrateur système, celui-ci est un langage de programmation interprété
"**impératif**". Il exécute les instructions les unes à la suite des autres.

Les langages dédiés au domaine (**indexterm2:[DSL]** Domain Specific Language)
comme ceux utilisés par Ansible, Terraform, ou Puppet, quant à eux, ne
spécifient pas les étapes à réaliser mais l'état à obtenir.

Parce qu'Ansible, Terraform ou Puppet utilisent un langage déclaratif, ils sont
très simples. Il suffit de leur dire "Fais cette action"
ou "Met le serveur dans cet état".
Ils considéreront l'état désiré indépendamment de l'état initial ou du contexte.
Peu importe dans quel état le serveur se situe au départ, les étapes à franchir
pour arriver au résultat, le serveur est mis dans l'état désiré avec un rapport
de succès (avec ou sans changement d'état) ou d'échec.

La même tâche d'automatisation en bash nécessiterait de vérifier tous les états
possibles, les autorisations, etc.
afin de déterminer la tâche à accomplir avant de lancer l'exécution de la
commande, souvent en imbriquant de nombreux "si", ce qui complique la tâche,
l'écriture et la maintenance du script.
****

== Les outils devops

* Outils de gestion de configuration

** Puppet
** Ansible
** Saltstack
** Chef

* Intégration continue

** Jenkins
** Gitlab-ci

* Orchestration des tâches

** Rundeck
** Ansible Tower

* Infrastructure As Code

** Terraform

* Docs as Code

** Asciidoctor
** Markdown
** ReSTructured Text
