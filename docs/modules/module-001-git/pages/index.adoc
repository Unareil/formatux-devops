= Premiers pas avec git
ifndef::env-site,env-github,backend-pdf[]
include::_attributes.adoc[]
endif::[]
ifdef::backend-pdf[]
:moduledir: {global_path}docs/modules/module-001-git
:imagesdir: {moduledir}/assets/images/
endif::[]
// Settings
:idprefix:
:idseparator: -


****
Ces articles ont été initialement rédigés par https://carlchenet.com/[Carl Chenet] et repris par formatux avec son aimable autorisation.
****

Git reste un programme incompris et craint par beaucoup alors qu’il est aujourd’hui indistinctement utilisé par les développeurs et les sysadmins. Afin de démystifier ce formidable outil, je vous propose une série d’articles à l’approche très concrète pour débuter avec Git et l’utiliser efficacement ensuite.

image::git-logo.png[scaledwidth="70%"]